function formatPrice(price) {
	if (!price) {
		return '0,00';
	}
	var formatedPrice = price.toFixed(2).toString().split('.');
	if(formatedPrice[1]) {
		if(formatedPrice[1].length < 2) {
			formatedPrice[1] += '0';
		}
	}
	else {
		formatedPrice[1] = '00';
	}

	return formatedPrice.join(',');
}

export default formatPrice;