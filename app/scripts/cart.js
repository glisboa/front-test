import _ from 'lodash';

var Cart = function (_products) {

  var calcTotalValue = function (_productsList) {
    if(_productsList.length > 1) {
      return _.reduce(_productsList, function (carr, curr) {
          return !!carr.price ? carr.price + parseFloat(curr.price) : carr + parseFloat(curr.price);
      });
    } else if(_productsList.length === 1) {
      return _productsList[0].price;
    }

    return 0;
  }

  var cart = {
    products: _products,

    totalValue: calcTotalValue(_products),

    installments: 10,

    addProduct: function (_cart, _product) {
      _cart.products.push(_product);
      _cart.totalValue = calcTotalValue(_products);
      localStorage.setItem("products", JSON.stringify(_cart.products));
    },

    removeProduct: function(_cart, _id) {
      _cart.products = _cart.products.splice(_.indexOf(_products, _id), 1);
      _cart.totalValue = calcTotalValue(_products);
      localStorage.setItem("products", JSON.stringify(_cart.products));
    }

  
  }

  return cart;
}

export default Cart;