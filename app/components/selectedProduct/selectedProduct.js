import formatPrice from './../../utils/values.js';

var selectedProduct = function (props) {
    var element = document.createElement('div');

    element.className = 'selected-product'

    var button = document.createElement('button');
    button.className = 'remove';

    element.innerHTML = '<div class="content"> \
      <img src="' + props.image + '" /> \
      <div class="info"> \
        <div class="text">\
        	<h5>\
            ' + props.title + '\
      		</h5>\
      		<p>\
      			' + props.availableSizes[0] + ' | ' + props.color + '<br />\
      			Quantidade: ' + props.count + '\
      		</p>\
        </div>\
        <p class="price">\
          <button class="remove"></button>\
        	<span>R$ '+ formatPrice(props.price) + '</span>\
        </p>\
      </div>\
    </div>';

    element.addEventListener('click', function() {
        props.onDelete(props.id);
    });

    return element;
}

export default selectedProduct;