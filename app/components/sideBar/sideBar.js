var sideBar = function (props) {
    var element = document.createElement('div');

    element.className = 'content';

    element.innerHTML = '<h2>\
        <span class="icon">\
          <img src="static/images/bag.png" role="presentation" />\
          <div class="count">\
            ' + props.itemCount + '\
          </div>\
        </span>\
        ' + props.title + '\
      </h2>\
      ' + props.children.outerHTML;

    return element;
}

export default sideBar;