import formatPrice from './../../utils/values.js';

var product = function (props) {

    var price = formatPrice(props.price);
    var intPrice = price.split(',')[0];
    var decimals = price.split(',')[1];

    var element = document.createElement('div');

    element.className = 'product'

    element.innerHTML = '<div class="content">\
      <img src="' + props.picture + '" alt="' + props.title + '" />\
      <h4 class="title">\
        ' + props.title + ' - ' + props.description + '\
      </h4>\
      <div class="separator"></div>\
      <p class="price">\
        <span>R$</span> ' + intPrice + ',<span>' + decimals + '</span>\
      </p>\
      <p class="installments">\
        ou ' + props.installments + ' X <span>R$ ' + formatPrice(props.installmentValue) + '</span>\
      </p>\
    </div>';

    element.addEventListener('click', function() {
        props.click(props.id);
    });

    return element;
}

export default product;