var header = function (props) {
    var element = document.createElement('div');

    element.className = 'header'

    element.innerHTML = '<div class="content">\
	  ' + props.children.outerHTML + '\
	</div>';

    return element;
}

export default header;