import formatPrice from './../../utils/values.js';

var cart = function (props) {
    var element = document.createElement('div');

    element.className = 'cart';

    element.innerHTML = '<div class="cart-content">\
          ' + props.children.outerHTML + '\
        </div>\
        <div class="summary">\
          <div class="info">\
          	<p class="total">\
          		SUBTOTAL\
          	</p>\
          	<p class="payment">\
          		<span class="payment-value">\
          			R$ ' + formatPrice(props.totalValue) + '\
          		</span>\
          		<span class="total-installments">\
          			OU EM ATÉ ' + props.installments + ' X R$ ' + formatPrice(props.totalValue / props.installments) + '\
          		</span>\
          	</p>\
          </div>\
          <button>COMPRAR</button>\
        </div>';

    return element;
}

export default cart;