import Product from './components/product/product';
import SideBar from './components/sideBar/sideBar';
import CartComponent from './components/cartComponent/cartComponent';
import SelectedProduct from './components/SelectedProduct/selectedProduct';

import Cart from './scripts/cart';

localStorage.setItem("lastname", "Smith");

var showcase = document.getElementById("showcase");

var cart = Cart(JSON.parse(localStorage.getItem("products")) || []);

var products = [];

var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function () {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
        products = JSON.parse(xhttp.responseText).products;

        for (var i = 0; i < products.length; i++) {
            var product = products[i];

            showcase.appendChild(Product({
                picture: 'static/images/1.jpg',
                title: product.title,
                description: product.description,
                style: product.style,
                price: product.price,
                installmentValue: product.price / cart.installments,
                installments: cart.installments,
                click: addProduct,
                id: product.id,
            }));
        }
    }
};

xhttp.open("GET", "/products", true);
xhttp.send();

function renderCart() {
    var productsHtml = document.createElement("p");
    for (var i = 0; i < cart.products.length; i++) {
        var product = cart.products[i];
        productsHtml.appendChild(SelectedProduct({
            image: 'static/images/1.jpg',
            title: product.title,
            availableSizes: product.availableSizes,
            color: product.style,
            count: product.length,
            price: product.price,
            onDelete: removeProduct,
            id: product.id,
        }));
    }

    document.getElementById("sideBar").innerHTML = '';

    document.getElementById("sideBar").appendChild(SideBar({
        title: "SACOLA",
        itemCount: cart.products.length,
        children: CartComponent({
            children: productsHtml,
            totalValue: cart.totalValue,
            installments: cart.installments,
        }),
    }));
}

document.getElementById('cartButton').addEventListener('click', function() {
    var sideBar = document.getElementById("sideBar");
    sideBar.className = sideBar.className.indexOf('visible') !== -1 ? 'side-bar' : 'side-bar visible';
});

renderCart();

function addProduct(_id) {
    cart.addProduct(cart, products[_id]);
    renderCart();
}

function removeProduct(_id) {
    cart.removeProduct(cart, products[_id]);
    renderCart();
}