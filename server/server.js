var express = require('express');
var path = require('path');
var app = express();
var fs = require('fs');

var products = JSON.parse(fs.readFileSync('./json.json', 'utf8'));

app.use('/static', express.static(path.resolve('app/dist/')));

app.get('/', (req, res) => {
    res.sendFile(path.resolve('app/index.html'));
});

app.get('/products', (req, res) => {
  res.send(products);
})

app.listen(3000, () => {
  console.log('http://localhost:3000');
});






